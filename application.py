
from flask import Flask 
from flask_mongoengine import MongoEngine


db = MongoEngine()


def create_app(**config_overrides):

    app = Flask(__name__)

    # Load config
    app.config.from_pyfile('settings.py')

    # Apply override for tests
    app.config.update(config_overrides)

    # Setup database
    db.init_app(app)

    # Import blueprints
    from category.views import category_app
    from home.views import home_app
    from episode.views import episode_app

    # Register blueprints
    app.register_blueprint(category_app)
    app.register_blueprint(home_app)
    app.register_blueprint(episode_app)

    return app
