#
# Category-API Dockerfile
#
#


# Pull base image.
FROM python

# Get some custom packages
RUN apt-get update && apt-get install -y \
    build-essential \
    make \
    gcc \
    python3-dev

# Make a local directory
#RUN mkdir /app/project-streamming/api-category


# Now copy all the fiels in this directory to /code
ADD . /tmp/project-streamming/api-category

# Set "categories-api" as the working directory from which CMD, RUN ,ADD references
WORKDIR /tmp/project-streamming/api-category

# pip install the local requiremnents.txt
RUN pip install -r requirements.txt

# Listen to port 3000 at runtime
EXPOSE 3000

# Start the app server
#CMD python manage.py
CMD python manage.py runserver


