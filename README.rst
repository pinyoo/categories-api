Category-API
Author:	Pinyoo thotaboot

About
Test microservice api on Docker by Flask Python and Mongodb

Tools
	- Python 
	- Docker 
	- Mongodb
	- Postman
	- VirtualBox
Setup
	Step:1
		youname$ docker-machine create --driver virtualbox manager
		youname$ docker-machine create --driver virtualbox worker-1
		youname$ docker-machine create --driver virtualbox worker-2
		
	Step:2
		youname$ docker-machine ls
			NAME       ACTIVE   DRIVER       STATE     URL   SWARM   DOCKER    ERRORS
			manager    -        virtualbox   Stopped                 Unknown   
			worker-1   -        virtualbox   Stopped                 Unknown   
			worker-2   -        virtualbox   Stopped                 Unknown
			
		youname$ docker-machine start manager
		youname$ docker-machine start worker-1
		youname$ docker-machine start worker-2
		
		youname$ docker-machine ls
		NAME       ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
		manager    -        virtualbox   Running   tcp://192.168.99.100:2376           v18.05.0-ce   
		worker-1   -        virtualbox   Running   tcp://192.168.99.101:2376           v18.05.0-ce   
		worker-2   -        virtualbox   Running   tcp://192.168.99.102:2376           v18.05.0-ce  
	
	Step:3
		youname$ docker-machine ssh manager
		 
		docker@manager:~$ 

	Step:4
		docker@manager:~$  docker swarm init --advertise-addr 192.168.99.100
		Swarm initialized: current node (siqyf3yricsvjkzvej00a9b8h) is now a manager.

		To add a worker to this swarm, run the following command:

    	docker swarm join \
    	--token SWMTKN-1-0eith07xkcg93lzftuhjmxaxwfa6mbkjsmjzb3d3sx9cobc2zp-97s6xzdt27y2gk3kpm0cgo6y2 \
    	192.168.99.100:2377

		To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
		
		youname$ docker-machine ssh worker-1
		
		[worker-1] $ docker swarm join --token SWMTKN-1-0eith07xkcg93lzftuhjmxaxwfa6mbkjsmjzb3d3sx9cobc2zp-97s6xzdt27y2gk3kpm0cgo6y2 192.168.99.100:2377
		This node joined a swarm as a worker.
		
		youname$ docker-machine ssh worker-2
		
		[worker-2] $ docker swarm join --token SWMTKN-1-0eith07xkcg93lzftuhjmxaxwfa6mbkjsmjzb3d3sx9cobc2zp-97s6xzdt27y2gk3kpm0cgo6y2 192.168.99.100:2377
		This node joined a swarm as a worker.
		
		
		[manager] $ docker node ls
		ID                           HOSTNAME  STATUS  AVAILABILITY  MANAGER STATUS
		j14mte3v1jhtbm3pb2qrpgwp6    worker-1  Ready   Active
		siqyf3yricsvjkzvej00a9b8h *  master    Ready   Active        Leader
		srl5yzme5hxnzxal2t1efmwje    worker-2  Ready   Active
	
	Step:5 . 
		[manager] $ docker service create --name backend --publish 8080:80 --replicas 2 nginx
		[manager] $ docker service ls
		ID            NAME    MODE        REPLICAS  IMAGE
		1okycpshfusq  backend  replicated  2/2       nginx:latest
	
	Step:6
		[manager] $ docker network create --driver overlay --subnet 10.24.90.0/24 mongo
	
	Step:7
		[manager] $ docker service create --name category-api --network mongo --update-delay 5s --publish 80:3000 --replicas 2 pinyoo/category:1
	
	
Testing
	
	youname$ curl -XGET 192.168.99.100
	{
	  "result": "Hello Category-api"
	}
	

Json Structure : category 

	{
		"name_en": "String",     
		"name_th": "String",     
    	"language": "String",
    	"sub_lang": "String",    
    	"description": "String", 
    	"num_ep": Int,      
    	"regis_date": Datetime,
    	"picture": "String",     
		"num_visit": Int,
    	"status": "่String"
	}
	

	
	
		
		
	
	
			
		
		
			
			
		
		
	