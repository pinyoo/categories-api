
from flask.views import MethodView
from flask import jsonify, request, abort, render_template
import uuid
import json
from jsonschema import Draft4Validator
from jsonschema.exceptions import best_match
import datetime

from episode.models import Episode
from episode.schema import schema
from episode.templates import episodes_obj, episode_obj

from category.models import Category


class EpisodeAPI(MethodView):

    def __init__(self):
        self.EPISODES_PER_PAGE = 10
        if (request.method != 'GET' and request.method != 'DELETE') and not request.json:
            abort(400)
    
    def get(self,episode_id):
        if episode_id:
            episode = Episode.objects.filter(external_id=episode_id,live=True).first()
            if episode:
                response = {
                    "result": "ok",
                    "episode": episode_obj(episode)
                }
                return jsonify(response), 200
            else:
                return jsonify({}), 404
        else:

            episode_href = "/episodes/?page=%s"
            episodes = Episode.objects.filter(live=True)

            if "type_video" in request.args:
                episodes = episodes.filter(type_video=request.args.get('type_video'))
                episode_href += "&type_video=" + request.args.get('type_video')
            
            if "name_ep" in request.args:
                episodes = episodes.filter(name_ep=request.args.get('name_ep'))
                episode_href += "&name_ep=" + request.args.get('name_ep')


            page = int(request.args.get('page',1))
            episodes = episodes.paginate(page=page,per_page=self.EPISODES_PER_PAGE)
            response = {
                "result": "ok",
                "links": [
                    {
                        "href": episode_href % page,
                        "rel": "self"
                    }
                ],
                "episodes": episodes_obj(episodes)
            }

            if episodes.has_prev:
                response["links"].append(
                    {
                        "href": episode_href % (episodes.prev_num),
                        "rel": "previous"
                    }
                )
            
            if episodes.has_next:
                response["links"].append(
                    {
                        "href": episode_href % (episodes.next_num),
                        "rel": "next"
                    }
                )
            return jsonify(response), 200

    def post(self):
        episode_json = request.json
        error = best_match(Draft4Validator(schema).iter_errors(episode_json))

        if error:
            return jsonify({"error": error.message}), 400
        
        category = Category.objects.filter(external_id=episode_json.get('category')).first()
        if not category:
            error = {
                "code":"CATEGORY_NOT_FOUND"
            }
            return jsonify({'error':error}), 400
        else:
            try:
                register_date = datetime.datetime.strptime(episode_json.get('regis_date'),"%Y-%m-%dT%H:%M:%SZ")
            except:
                return jsonify({"error":"INVALID_DATE"}), 400

            episode = Episode(
                external_id = str(uuid.uuid4()),
                type_video =  episode_json.get('type_video'),
                category = category,
                name_ep =  episode_json.get('name_ep'),
                path_link = episode_json.get('path_link'),
                regis_date = register_date
            ).save()

            response = {
                "result": "ok",
                "episode": episode_obj(episode)
            }
            return jsonify(response), 201
    
    def put(self,episode_id):
        episode = Episode.objects.filter(external_id=episode_id,live=True).first()
        if not episode:
            return jsonify({}), 404
        episode_json = request.json
        error = best_match(Draft4Validator(schema).iter_errors(episode_json))

        if error:
            return jsonify({"error": error.message}), 400
        else:
            category = Category.objects.filter(external_id=episode_json.get('category')).first()
            if not category:
                error = {
                    "code":"CATEGORY_NOT_FOUND"
                }
                return jsonify({'error':error}), 400
            try:
                register_date = datetime.datetime.strptime(episode_json.get('regis_date'),"%Y-%m-%dT%H:%M:%SZ")
            except:
                return jsonify({"error":"INVALID_DATE"}), 400

            episode.type_video  = episode_json.get('type_video')
            episode.category    = category
            episode.name_ep     = episode_json.get('name_ep')
            episode.path_link   = episode_json.get('path_link')
            episode.regis_date  = register_date
            episode.save()
            response = {
                "result": "ok",
                "episode": episode_obj(episode,nocategory=False)
            }

            return jsonify(response), 200
    
    def delete(self,episode_id):
        episode = Episode.objects.filter(external_id=episode_id,live=True).first()

        if not episode:
            return jsonify({}), 404
        
        episode.live = False
        episode.save()

        return jsonify({}), 204

        
