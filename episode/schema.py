schema = {
    "type": "object",
    "properties": {
        "type_video": { "type": "string" },     
        "category":   { "type": "string" },     
        "name_ep":    { "type": "string" },
        "path_link":  { "type": "string" },    
        "regis_date": { "type": "string", "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$" }       
    },
    "required": ["type_video","category","name_ep","path_link","regis_date"]
}
