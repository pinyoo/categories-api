from flask import Blueprint

from episode.api import EpisodeAPI


episode_app = Blueprint('episode_app',__name__)

episode_view = EpisodeAPI.as_view('episode_api')

episode_app.add_url_rule('/episodes/',defaults={'episode_id': None},
                    view_func = episode_view, methods=['GET',])

episode_app.add_url_rule('/episodes/<episode_id>',view_func=episode_view,
                        methods=['GET','PUT','DELETE',])

episode_app.add_url_rule('/episodes/',view_func=episode_view,methods=['POST',])