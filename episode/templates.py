
from category.templates import category_obj

def episode_obj(episode,nocategory=False):
    episode_obj= {
        "id":           episode.external_id,
        "type_video":   episode.type_video, 
        "name_ep":      episode.name_ep, 
        "path_link":    episode.path_link,
        "regis_date":   str(episode.regis_date.isoformat()[:19]) + "Z",
        "links": [
            { "rel": "self", "href": "/episodes/" + episode.external_id }
        ]
    }
    if not nocategory:
        episode_obj["category"] = category_obj(episode.category)
    
    return episode_obj

def episodes_obj(episodes,nocategory=False):
    episodes_obj = []
    for episode in episodes.items:
        episodes_obj.append(episode_obj(episode,nocategory))
    return episodes_obj