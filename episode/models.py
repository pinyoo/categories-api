from mongoengine import signals

from application import db
from category.models import Category

class Episode(db.Document):
    external_id = db.StringField(db_field="ei")
    type_video  = db.StringField(db_field="tv")
    category    = db.ReferenceField(Category,db_field="ca")
    name_ep     = db.StringField(db_field="ne")
    path_link   = db.StringField(db_field="pl")
    regis_date  = db.DateTimeField(db_field="rd")           # วันที่ลงทะเบียน
    live        = db.BooleanField(db_field="li",default=True)
