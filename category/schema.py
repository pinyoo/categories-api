schema = {
    "type": "object",
    "properties": {
        "name_en":     { "type": "string" },     
        "name_th":     { "type": "string" },     
        "language":    { "type": "string" },
        "sub_lang":    { "type": "string" },    
        "description": { "type": "string" }, 
        "num_ep":      { "type": "number" },      
        "regis_date":  { "type": "string", "pattern": "^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$" },   
        "picture":     { "type": "string" },     
        "num_visit":   { "type": "number" },
        "status":      { "type": "string" }        
    },
    "required": ["name_en","name_th","language","sub_lang","description","num_ep","regis_date","picture","num_visit","status"]
}