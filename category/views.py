from flask import Blueprint

from category.api import CategoryAPI



category_app = Blueprint('category_app',__name__)

category_view = CategoryAPI.as_view('category_api')

category_app.add_url_rule('/categories/',defaults={'category_id': None},
                    view_func = category_view, methods=['GET',])

category_app.add_url_rule('/categories/<category_id>',view_func=category_view,
                        methods=['GET','PUT','DELETE',])

category_app.add_url_rule('/categories/',view_func=category_view,methods=['POST',])

category_app.add_url_rule('/categories/<category_id>/episodes/',view_func=category_view,
                            methods=['GET',])