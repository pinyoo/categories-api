from mongoengine import signals


from application import db

class Category(db.Document):
    external_id = db.StringField(db_field="ei",unique=True) # เก็บรหัส category
    name_en     = db.StringField(db_field="ne")             # เก็บชื่อเรื่องภาษาอังกฤษ
    name_th     = db.StringField(db_field="nt")             # เก็บชื่อเรื่องภาษาไทย
    language    = db.StringField(db_field="la")             # พากษ์ภาษา
    sub_lang    = db.StringField(db_field="sl")             # ซับไตเติ้ล
    description = db.StringField(db_field="de")             # เนื้อเรื่องโดยย่อ
    num_ep      = db.IntField(db_field="np")                # จำนวนตอน
    regis_date  = db.DateTimeField(db_field="rd")           # วันที่ลงทะเบียน
    picture     = db.StringField(db_field="pi")             # link path picture
    num_visit   = db.IntField(db_field="nv")                # จำนวนคนเข้าดูหมวดนี้
    status      = db.StringField(db_field="st")             # สถานะวีดีโอ จบแล้ว,   ยังไม่จบ
    live        = db.BooleanField(db_field="li",default= True)  # True = ยังไม่ลบ , False = ลบแล้ว
   