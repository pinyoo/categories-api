
from flask.views import MethodView
from flask import jsonify, request, abort, render_template
import uuid
import json
from jsonschema import Draft4Validator
from jsonschema.exceptions import best_match
import datetime
from category.models import Category
from category.schema import schema
from category.templates import categories_obj, category_obj

from episode.models import Episode
from episode.templates import episodes_obj

class CategoryAPI(MethodView):

    def __init__(self):
        self.CATEGORIES_PER_PAGE = 10
        self.EPISODES_PER_PAGE = 10
        if (request.method != 'GET' and request.method != 'DELETE') and not request.json:
            abort(400)
    
    def get(self,category_id):
        if category_id:
            category = Category.objects.filter(external_id=category_id,live=True).first()
            if category:
                if "episodes" in request.url:
                    episodes= Episode.objects.filter(category=category,live=True)
                    page = int(request.args.get('page',1))
                    episodes = episodes.paginate(page=page,per_page= self.EPISODES_PER_PAGE)
                    response = {
                        "result": "ok",
                        "links": [
                            {
                                "href": "/categories/%s/episodes/?page=%s" % (category_id,page),
                                "rel": "self"
                            }
                        ],
                        "category": category_obj(category),
                        "episodes": episodes_obj(episodes,nocategory=True)
                    }

                    if episodes.has_prev:
                        response["links"].append(
                            {
                                "href": "/categories/%s/episodes/?page=%s" % (category_id,episodes.prev_num),
                                "rel": "previous"
                            }
                        )
                    if episodes.has_next:
                        response["links"].append(
                            {
                                "href": "/categories/%s/episodes/?page=%s" %(category_id,episodes.next_num),
                                "rel": "next"
                            }
                        )
                else:
                    response = {
                        "result": "ok",
                        "category": category_obj(category)
                    }

                return jsonify(response), 200
            else:
                return jsonify({}), 404
        else:
            categories = Category.objects.filter(live=True)
            page = int(request.args.get('page',1))
            categories = categories.paginate(page=page,per_page=self.CATEGORIES_PER_PAGE)
            response = {
                "result": "ok",
                "links": [
                    {
                        "href": "/categories/?page=%s" % page,
                        "rel": "self"
                    }
                ],
                "categories": categories_obj(categories)
            }

            if categories.has_prev:
                response["links"].append(
                    {
                        "href": "/categories/?page=%s" % (categories.prev_num),
                        "rel": "previous"
                    }
                )
            
            if categories.has_next:
                response["links"].append(
                    {
                        "href": "/categories/?page=%s" % (categories.next_num),
                        "rel": "next"
                    }
                )
            return jsonify(response), 200

    def post(self):
        category_json = request.json
        error = best_match(Draft4Validator(schema).iter_errors(category_json))

        if error:
            return jsonify({"error": error.message}), 400
        else:
            try:
                register_date = datetime.datetime.strptime(category_json.get('regis_date'),"%Y-%m-%dT%H:%M:%SZ")
            except:
                return jsonify({"error":"INVALID_DATE"}), 400

            category = Category(
                external_id = str(uuid.uuid4()),
                name_en =  category_json.get('name_en'),
                name_th =  category_json.get('name_th'),
                language = category_json.get('language'),
                sub_lang = category_json.get('sub_lang'),
                description = category_json.get('description'),
                num_ep =  category_json.get('num_ep'),
                regis_date = register_date,
                picture =  category_json.get('picture'),
                num_visit = category_json.get('num_visit'),
                status = category_json.get('status')
            ).save()

            response = {
                "result": "ok",
                "category": category_obj(category)
            }

            return jsonify(response), 201

    def put(self,category_id):
        category = Category.objects.filter(external_id=category_id,live=True).first()
        if not category:
            return jsonify({}), 404
        category_json = request.json
        error = best_match(Draft4Validator(schema).iter_errors(category_json))

        if error:
            return jsonify({"error": error.message}), 400
        else:
            try:
                register_date = datetime.datetime.strptime(category_json.get('regis_date'),"%Y-%m-%dT%H:%M:%SZ")
            except:
                return jsonify({"error":"INVALID_DATE"}), 400

            category.name_en =  category_json.get('name_en')
            category.name_th =  category_json.get('name_th')
            category.language = category_json.get('language')
            category.sub_lang = category_json.get('sub_lang')
            category.description = category_json.get('description')
            category.num_ep =  category_json.get('num_ep')
            category.regis_date = register_date
            category.status = category_json.get('status')
            category.picture =  category_json.get('picture')
            category.num_visit = category_json.get('num_visit')
            category.save()

            response = {
                "result": "ok",
                "category": category_obj(category)
            }

            return jsonify(response), 200

    def delete(self,category_id):
        category = Category.objects.filter(external_id=category_id,live=True).first()

        if not category:
            return jsonify({}), 404
        
        category.live = False
        category.save()

        return jsonify({}), 204

