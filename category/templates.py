def category_obj(category):
    return {
        "id":             category.external_id,
        "name_en":        category.name_en, 
        "name_th":        category.name_th,
        "language":       category.language,
        "sub_lang":       category.sub_lang, 
        "description":    category.description,
        "num_ep":         str(category.num_ep),
        "regis_date":     str(category.regis_date.isoformat()[:19]) + "Z",
        "picture":        category.picture,
        "num_visit":      str(category.num_visit),
        "status":         category.status,
        "links": [
            { "rel": "self", "href": "/categories/" + category.external_id },
            { "rel": "episodes", "href": "/categories/%s/episodes/" % category.external_id}
        ]
    }

def categories_obj(categories):
    categories_obj = []
    for category in categories.items:
        categories_obj.append(category_obj(category))
    return categories_obj