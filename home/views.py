
from flask import Blueprint, jsonify

home_app = Blueprint('home_app',__name__)

@home_app.route('/')
def home():
    response = {
        "result": "Hello Category-api"
    }
    return jsonify(response), 200